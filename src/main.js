import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import router from './router'
import axios from 'axios'

Vue.use(Vuetify)
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  render: h => h(App)
});


router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    if (checkAuth()) {
      next();
    } else {
      next('Signin');
    }
  } else {
    next();
  }
});

function checkAuth() {
  const token = localStorage.getItem('moneto_token');
  if (token) {
    return true;
  }
  return false;
}
