import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Receipts from '@/components/Receipts'
import Clients from '@/components/Clients'
import Signin from '@/components/Signin'
import Signup from '@/components/Signup'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: { requiresAuth: true }
    },
    {
      path: '/receipts',
      name: 'Receipts',
      component: Receipts,
      meta: { requiresAuth: true }
    },
    {
      path: '/clients',
      name: 'Clients',
      component: Clients,
      meta: { requiresAuth: true }
    },
    {
      path: '/signin',
      name: 'Signin',
      component: Signin,
      meta: { requiresAuth: false }
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup,
      meta: { requiresAuth: true }
    }
  ],
  mode: 'history'
})
