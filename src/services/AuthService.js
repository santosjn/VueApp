export default {

	methods: {		
		getAuthToken: function(){
			return localStorage.getItem('moneto_token');
		},
		setAuthToken: function(token){
			return localStorage.setItem('moneto_token', token);			
		}
	}
}

